const theme = localStorage.getItem('theme');
if (theme) {
    document.body.classList.add(theme);
}

function toggle() {
    const body = document.body;

    if (body.classList.contains('dark')) {
        body.classList.remove('dark');
        localStorage.setItem('theme', 'light');
    } else {
        body.classList.add('dark');
        localStorage.setItem('theme', 'dark');
    }
}

const toggleBtn = document.getElementById('theme');
toggleBtn.addEventListener('click', toggle);
